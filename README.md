#include <stdio.h> 
#include <pthread.h>           
 
Producer() 
  { 
    int widget; 
 
    while (TRUE) {                   
      make_new(widget);              
      down(&empty);                  
      down(&mutex);                  
      put_item(widget);              
      up(&mutex);                    
      up(&full);  
      } 
  } 
 
  Consumer() 
  { 
    int widget; 
       
    while (TRUE) {    
      down(&full);              
      down(&mutex);       
      remove_item(widget);     
      up(&mutex);  
      up(&empty);  
      consume_item(widget);  
      } 
  }